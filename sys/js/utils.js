function pause() {
	if(timer) {
		clearTimeout(timer);
		timer = null;
	} else timer = setInterval(doPhys,dt*1000);
}

function mark(object) {
	const marker = document.createElement('div');
	marker.style.position='absolute';
	marker.style.left=object.x-object.r+'px';
	marker.style.bottom=object.y-object.r+'px';
	marker.style.width=object.r*2+'px';
	marker.style.border='solid 1px red';
	marker.style.height=object.r*2+'px';
	document.body.appendChild(marker);
}

function stop() {
	clearTimeout(timer);
	timer = null;
}

function getId(id) {
	return document.getElementById(id);
}