const vm = new Vue({
	el: '#app',
	data: {
		config: {
			num: 20,
			minR: 5,
			maxR: 35,
			dt: 0.02,
		},
	},
	methods: {
		showConf: showConf,
		pause: pause,
		add: ()=>{createRandom();draw();},
		del: ()=>{objects.pop();draw();},
	}
});

function showConf() {
	getId('configDial').showModal();
}

function canvasInit() {
	canvas = document.createElement('canvas');
	canvas.height = HEIGHT;
	canvas.width = WIDTH;
	canvas.id = 'canvas';
	canvas.style.position = 'absolute';
	canvas.style.top = '0';
	canvas.style.left = '0';
	canvas.addEventListener('click',showInf);
	document.body.appendChild(canvas);
	context = canvas.getContext("2d");
	timer = setInterval(doPhys,dt*1000);
}

function draw() {
	context.fillStyle = "#121519";
	context.fillRect(0, 0, WIDTH, HEIGHT);
	context.fillStyle = "#000000";
	for(let object of objects){
		context.beginPath();
		context.arc(
			object.x,
			HEIGHT-object.y,
			object.r,
			0,
			Math.PI * 2
		);
		context.closePath();
		context.fill();
	}
}

function createRandom() {
	const r = Math.round(Math.random()*30+5);
	const x = Math.random()*(WIDTH-r*2);
	const y = Math.random()*(HEIGHT-r*2);
	const speedY = Math.random()*200-100;
	const speedX = Math.random()*200-100;
	new physObject(x,y,r,r*r,speedX,speedY);
}
for(let i = 0;i<20;i++) {
	createRandom();
}

function showInf(e) {
	const x = e.pageX;
	const y = HEIGHT-e.pageY;
	objects.forEach((object)=>{
		const dx = x-object.x;
		const dy = y-object.y;
		if(dx*dx+dy*dy<=object.r*object.r) {
			stop();
			console.log(object);
		}
	});
}

// new physObject(300,30,10,30,-50,0);
// new physObject(100,50,30,60,0,0);

window.addEventListener('resize',()=>{
	HEIGHT = window.innerHeight;
	WIDTH = window.innerWidth;
	canvas.height=HEIGHT;
	canvas.width=WIDTH;
	draw();
});

document.addEventListener("DOMContentLoaded", canvasInit, true);