let HEIGHT = window.innerHeight;
let WIDTH = window.innerWidth;
let dt = 0.02;
const objects = [];

let context, canvas;
let timer;

class Listener {
	constructor(type, callback, bubble) {
		this.type = type;
		this.callback = callback;
		this.bubble = bubble;
	}
}

class physObject {
	constructor(x,y,r=30,m=10,speedX=-5,speedY=-5) {
		this.x = x;
		this.y = y;
		this.r = r;
		this.m = m;//Задаем всевозможные параметры
		this.speedX = speedX;
		this.speedY = speedY;
		this.listeners = [];//массив слушателей
		objects.push(this);//пушим созданный объект в массив физ объектов
	}

	get speed() {//получаем скаляр скорости
		return Math.sqrt(this.speedX*this.speedX+this.speedY*this.speedY);
	}

	get Coords() {//Получаем объект с координатами
		return {x: this.x, y: this.y};
	}

	set Coords({x: x,y: y}) {//корректируем координаты если они установлены
		//за полем прорисовки(эффект отскока от стен)
		if(x<=this.r) {
			x = this.r;
			this.speedX = -this.speedX;
		} else if(x>=WIDTH-this.r) {
			x = WIDTH-this.r;
			this.speedX = -this.speedX;
		}
		if(y<=this.r) {
			y = this.r;
			this.speedY= -this.speedY;
		} else if(y>=HEIGHT-this.r) {
			y = HEIGHT-this.r;
			this.speedY= -this.speedY;
		}
		this.x=x;
		this.y=y;
	}

	isTouch(object) {//проверка касается ли другой объект этого
		const dx = object.x-this.x;
		const dy = object.y-this.y;
		return ((object.r+this.r)*(object.r+this.r))>=(dx*dx+dy*dy);
	}

	addListener(type,callback,bubble=false) {
		this.listeners.push(new Listener(type,callback,bubble));
	}
}

function vectors(o1,o2) {//получение проекций скоростей обоих объектов
	//после столкновения
	const vGet = (o1,o2) => {
		const dx = o2.x-o1.x;
		const dy = o2.y-o1.y;
		const dist = Math.sqrt(dx*dx+dy*dy);
		const cosF = dx/dist;
		const sinF = dy/dist;
		const a = ((o1.speedX*cosF+o1.speedY*sinF)*(o1.m-o2.m)+2*o2.m*(o2.speedX*cosF+o2.speedY*sinF))/(o1.m+o2.m);
		const b = o1.speedY*cosF-o1.speedX*sinF;
		const x = a*cosF+b*(-sinF);
		const y = a*sinF+b*cosF;
		return {x: x,y: y};
	}

	const result = [];

	result.push(vGet(o1,o2));
	result.push(vGet(o2,o1));

	return result;
}

function touching(object1,object2) {
	const listeners = object1.listeners.concat(object2.listeners);

	for(const listener of listeners.filter(listener => listener.type==='startTouch')) {
		listener.callback(object1,object2);
	}

	const vect = vectors(object1,object2);

	object1.speedX = vect[0].x;
	object1.speedY = vect[0].y;
	object2.speedX = vect[1].x;//присваиваем скорости
	object2.speedY = vect[1].y;

	let b;
	let s;

	if(object1.r>object2.r) {//дальше корректировка если один объект в другом
		b=object1;//то нужно изменить координаты
		s=object2;//"Отталкиваем" меньший объект т.к. это менее заметно
	} else {
		b=object2;
		s=object1;
	}
	
	const dx = b.x-s.x;
	const dy = b.y-s.y;
	const dist = Math.sqrt(dx*dx+dy*dy);

	s.x = b.x - dx/dist*(b.r+s.r);
	s.y = b.y - dy/dist*(b.r+s.r);

	for(const listener of listeners.filter(listener => listener.type==='endTouch')) {
		listener.callback(object1,object2);
	}
}

function doPhys() {
	for(const object of objects) {
		const Coords = object.Coords;
		Coords.x+=object.speedX/10;
		Coords.y+=object.speedY/10;
		object.Coords=Coords;
	}
	for(let i = 0;i<objects.length;i++) {
		for(let j = i;j<objects.length;j++) {
			if(objects[i]!==objects[j]&&objects[i].isTouch(objects[j])) touching(objects[i],objects[j]);
		}
	}
	draw();
}